pd-nusmuk (20151113+repack-9) unstable; urgency=medium

  * Patch to use pd_error() instead of error() (Closes: #1066588)
  * Bump standards version to 4.6.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 18 Mar 2024 12:26:56 +0100

pd-nusmuk (20151113+repack-8) unstable; urgency=medium

  * Modernize 'licensecheck' target
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 05 Dec 2022 10:56:41 +0100

pd-nusmuk (20151113+repack-7) unstable; urgency=medium

  * Properly pass CPPFLAGS to build
  * Patch to properly include <endian.h> on non-linux
  * Apply "warp-and-sort -ast"
  * Bump dh-compat to 13
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 04 Sep 2022 14:11:08 +0200

pd-nusmuk (20151113+repack-6) unstable; urgency=medium

  * Fix FTCBFS: Let dh_auto_build pass cross tools to make.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #958278)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 20 Apr 2020 18:36:53 +0200

pd-nusmuk (20151113+repack-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Debian Janitor ]
  * Use secure URI in debian/watch.
  * Bump debhelper from old 11 to 12.

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Declare that building this package does not require 'root' powers
  * Add salsa-ci configuration
  * Drop obsolete file d/source/local-options
  * Bump standards version to 4.5.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 13 Apr 2020 21:21:21 +0200

pd-nusmuk (20151113+repack-4) unstable; urgency=medium

  * Switched buildsystem from dh to cdbs
    * Bumped dh compat to 11
    * Enabled hardening
    * Dropped unneeded B-Ds
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Removed trailing whitespace in debian/*
  * Removed obsolete git-tuneclone.sh script
    * Updated d/README.source
  * Switched URLs to https:/
  * Updated d/copyright_hints
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:25:25 +0100

pd-nusmuk (20151113+repack-3) unstable; urgency=medium

  * Enabled hardening
  * Fixed typos
  * Updated d/copyright(_hints)
  * Canonical Vcs-* stanzas
  * Used multiple lines for multivalued stanzas
  * Bumped standards version to 3.9.8

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 06 Jan 2017 22:50:51 +0100

pd-nusmuk (20151113+repack-2) unstable; urgency=medium

  * Fixed DESTDIR installation
  * Switched to CDBS
  * Fixed maintainer: pkg-multimedia-maintainers
  * Provide pd-nusmuk-(audio|utils)
  * Added debian/git-tuneclone.sh (documented in README.source)
  * Added gbp.conf
  * Updated debian/copyright
  * Added local source options
  * Clarified repack
  * Bumped standards-version to 3.9.7

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 04 Feb 2016 22:25:35 +0100

pd-nusmuk (20151113+repack-1) unstable; urgency=medium

  * Initial release (Closes: #804712)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 18 Nov 2015 00:06:18 +0100
